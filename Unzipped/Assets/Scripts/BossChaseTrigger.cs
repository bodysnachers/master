﻿using UnityEngine;
using System.Collections;

public class BossChaseTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        //IF PLAYER IS IN CHASE RANGE BEGIN CHASING
        var boss = GameObject.Find("Boss");
        if (other.gameObject.name == "Player")
            boss.GetComponent<BossAI>().isChasing = true;
    }

    void OnTriggerExit(Collider other)
    {
        //IF PLAYER IS OUT OF CHASE RANGE STOP CHASING
        var boss = GameObject.Find("Boss");
        if (other.gameObject.name == "Player")
            boss.GetComponent<BossAI>().isChasing = false;
    }
}
