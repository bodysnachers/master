﻿using UnityEngine;
using System.Collections;

public class DoorClose : MonoBehaviour {

    private Vector3 origPos;
    private Ghostpieces pieces;
    private PlayerController player;
	private bool doorOpend = true;

    void Start()
    {
        player = GetComponent<PlayerController>();
        pieces = GetComponent<Ghostpieces>();
        origPos = transform.position;
    }

    void Update()
    {
        var pieces = GameObject.Find("GhostBody");
        var player = GameObject.Find("Player");
        //IF ALL GHOSTPIECES HAVE BEEN COLLECTED DOOR WILL BE OPEN
		if (pieces.GetComponent<Ghostpieces> ().collected == 5 && player.GetComponent<PlayerController> ().doorClosed == false && doorOpend == true) {
			transform.position = new Vector3 ((transform.position.x + 14), transform.position.y, transform.position.z);
			doorOpend = false;
		}

        //CLOSES DOOR WHEN IN BOSS ARENA
        if (player.GetComponent<PlayerController>().doorClosed == true)
            transform.position = origPos;

        //OPENS DOOR BACK UP IF DIED
        if (player.GetComponent<PlayerController>().isDead == true && pieces.GetComponent<Ghostpieces>().collected == 5)
            transform.position = new Vector3((transform.position.x + 14), transform.position.y, transform.position.z);

    }
}
