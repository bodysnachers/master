﻿using UnityEngine;
using System.Collections;

public class AnimationScript : MonoBehaviour {

	public Animator anim;
	public PlayerController playerSkin;
	private string currentMode;

	// Use this for initialization
	void Start () {
			
		anim = GetComponent<Animator> ();	
	}

    // Update is called once per frame
    void Update() {
        if (currentMode == "Doll")
        {
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
                anim.SetBool("DollWalk", true);
            else
                anim.SetBool("DollWalk", false);
        }

        if (currentMode == "Frog")
        {
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
                anim.SetBool("FrogWalk", true);
            else
                anim.SetBool("FrogWalk", false);
        }

        if (currentMode == "Dog")
        {
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
                anim.SetBool("DogWalk", true);//Because its using FrogAnim
            else
                anim.SetBool("DogWalk", false);//Because its using FrogAnim
        }

        if (currentMode == "Cat")
        {
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
                anim.SetBool("CatWalk", true);//Because its using FrogAnim
            else
                anim.SetBool("CatWalk", false);//Because its using FrogAnim
        }

        if (playerSkin.frogMode == true && currentMode != "Frog") {
			anim.SetTrigger ("Frog");
			currentMode = "Frog";
		}
		if (playerSkin.dollMode == true && currentMode != "Doll") {
			anim.SetTrigger ("Doll");
			currentMode = "Doll";
		} 
		if(playerSkin.defaultMode == true && currentMode != "Ghost") {
			currentMode = "Ghost";
		}
        if (playerSkin.dogMode == true && currentMode != "Dog")
        {
            anim.SetTrigger("Dog");//Because its using FrogAnim
            currentMode = "Dog";
        }
        if (playerSkin.catMode == true && currentMode != "Cat")
        {
            anim.SetTrigger("Cat");//Because its using FrogAnim
            currentMode = "Cat";
        }


    }




	
	
	
}


