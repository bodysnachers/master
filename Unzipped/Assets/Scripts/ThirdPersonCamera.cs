﻿using UnityEngine;
using System.Collections;

public class ThirdPersonCamera : MonoBehaviour {

    private const float Y_ANGLE_MIN = 20.0f;
    private const float Y_ANGLE_MAX = 50.0f;

    public Transform lookAt;
    public Transform camTransform;


    private Camera cam;
    public float distance = 10.0f;
    [HideInInspector]
    public float currentX = 0.0f;
    private float currentY = 0.0f;
    private float sensitivityX = 10.0f;
    private float sensitivityY = 3.0f;
	private bool camSwitch = false;


    private void Start()
    {
        camTransform = transform;
        cam = Camera.main;


    }

    private void Update()
    {
        //DETERMINES PLAYER FORWARD POSITION
        var player = GameObject.Find("Player");
        player.transform.eulerAngles = new Vector3(0, currentX, 0);


        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
			if (Input.GetMouseButton (1)) {
				currentX += Input.GetAxis ("Mouse X") * 2;
				currentY += Input.GetAxis ("Mouse Y");
			}
			currentX += Input.GetAxis ("Joystick X") * 2;
			currentY += Input.GetAxis ("Joystick Y");
		}

		if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor) {
			currentX += Input.GetAxis ("Mouse X") * 2;
			currentY += Input.GetAxis ("Mouse Y");

			currentX += Input.GetAxis ("Joystick XOSX") * 2;
			currentY += Input.GetAxis ("Joystick YOSX");

		}

            currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);

        if (Input.GetKeyDown(KeyCode.RightControl))
        { 
            camSwitch = !camSwitch;
        }
    }

    private void LateUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        camTransform.position = lookAt.position + rotation * dir;
		if(Input.GetButton("Vertical") && camSwitch == true)
			lookAt.transform.rotation = Quaternion.Euler(0,currentX,0);
        camTransform.LookAt(lookAt.position);
    }
}
