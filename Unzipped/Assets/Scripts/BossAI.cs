﻿using UnityEngine;
using System.Collections;

public class BossAI : MonoBehaviour {

    public Transform playerPos;

    [HideInInspector]
    public bool isChasing = false;
    [HideInInspector]
    public bool inAttackRange = false;
    [HideInInspector]
    public bool isAttacking = false;

    private float rotSpeed = 3.0f;
    private float movSpeed = 3.0f;
    private Vector3 origPos;
    private Quaternion origRot;
    private Animator anim;
	public bool win = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        origPos = transform.position;
        origRot = transform.rotation;
        playerPos = GameObject.Find("Player").transform;
    }

    void OnTriggerEnter(Collider other)
    {
        //IF BOULDER TOUCHES BOSS HE DIES
		var bossComp = GameObject.FindGameObjectWithTag("Boss");
		if (other.gameObject.name == "DeathTouch") {
			bossComp.SetActive (false);
			win = true;

		}
		
    }

    void Update()
    {
        //ANIMATE IDLE
        if (isChasing == false)
            anim.SetBool("Chase", false);

        //ANIMATE WALK
        if (isChasing == true)
            anim.SetBool("Chase", true);

        //CHASING PLAYER
        if (isChasing == true && inAttackRange == false && isAttacking == false)
        {
            //LOOKS AT PLAYER
            transform.rotation = 
                Quaternion.Slerp(transform.rotation, 
                Quaternion.LookRotation(playerPos.position - transform.position), 
                rotSpeed * Time.deltaTime);

            //CHASES PLAYER
            transform.position += transform.forward * movSpeed * Time.deltaTime;
        }
        
        //IN RANGE
        if(inAttackRange == true)
        {
            Debug.Log("Attack starting get out of range");
            isAttacking = true;
            StartCoroutine(AttackWait());
        }

        //IF PLAYER DIES BOSS POSITION IS RESET
        var player = GameObject.Find("Player");
        if(player.GetComponent<PlayerController>().isDead == true)
        {
            transform.position = origPos;
            transform.rotation = origRot;
        }

    }

    IEnumerator AttackWait()
    {
        yield return new WaitForSeconds(3f);
        //IF PLAYER IS IN ATTACK RANGE AFTER 3 SECONDS PLAYER DIES
        if (inAttackRange == true) {
            var player = GameObject.Find("Player");
            player.GetComponent<PlayerController>().health += 50f;
                }
        Debug.Log("Attack complete");
        isAttacking = false;

    }

}
