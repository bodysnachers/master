﻿using UnityEngine;
using System.Collections;

public class JoystickCamera : MonoBehaviour {
	public GameObject Target;
	public float RotateSpeed = 170,
	FollowDistance = 20,
	FollowHeight = 10;
	float RotateSpeedPerTime,
	DesiredRotationAngle,
	DesiredHeight,
	CurrentRotationAngle,
	CurrentHeight,
	Yaw,
	Pitch;
	Quaternion CurrentRotation;	
	float MoveSpeed = 50f;


		float DeltaTime;

		void Update()
		{
			DeltaTime = Time.deltaTime;
			transform.Rotate(0, Input.GetAxis("LeftX") * RotateSpeed * DeltaTime, 0);
			transform.Translate(0, 0, -Input.GetAxis("LeftY") * MoveSpeed * DeltaTime);
		}

	


	void LateUpdate()
		{
			RotateSpeedPerTime = RotateSpeed * Time.deltaTime;

			DesiredRotationAngle = Target.transform.eulerAngles.y;
			DesiredHeight = Target.transform.position.y + FollowHeight;
			CurrentRotationAngle = transform.eulerAngles.y;
			CurrentHeight = transform.position.y;

			CurrentRotationAngle = Mathf.LerpAngle(CurrentRotationAngle, DesiredRotationAngle, 0);
			CurrentHeight = Mathf.Lerp(CurrentHeight, DesiredHeight, 0);

			CurrentRotation = Quaternion.Euler(0, CurrentRotationAngle, 0);
			transform.position = Target.transform.position;
			transform.position -= CurrentRotation * Vector3.forward * FollowDistance;
			transform.position = new Vector3(transform.position.x, CurrentHeight, transform.position.z);

			Yaw = Input.GetAxis("RightX") * RotateSpeedPerTime;
			Pitch = Input.GetAxis("RightY") * RotateSpeedPerTime;
			transform.Translate(new Vector3(Yaw, -Pitch, 0));
			transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);

			transform.LookAt(Target.transform);
		}
}

