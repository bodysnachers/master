﻿using UnityEngine;
using System.Collections;

public class BridgeCollapse : MonoBehaviour {

    private Vector3 origPos;
    private Quaternion origRot;

    void Start()
    {
        origPos = transform.position;
        origRot = transform.rotation;
    }
	public void OnCollisionEnter(Collision other)
    {
        //WHEN PLAYER TOUCHES BRIDGE, PLANK FALLS
        if (other.gameObject.name == "Player")
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
    }

    void Update()
    {
        //RESETS BRIDGE IF PLAYER DIES
        var you = GameObject.Find("Player");
        if (you.GetComponent<PlayerController>().isDead == true)
        {
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            transform.position = origPos;
            transform.rotation = origRot;
        }
    }
}
