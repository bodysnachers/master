﻿using UnityEngine;
using System.Collections;

public class RotateCamera : MonoBehaviour {
	public float speed;
	public GameObject target;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		transform.RotateAround(target.transform.position, Vector3.up, speed * Time.deltaTime);



	}
}
