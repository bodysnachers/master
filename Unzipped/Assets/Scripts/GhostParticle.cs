﻿using UnityEngine;
using System.Collections;
using System;

public class GhostParticle : MonoBehaviour {

    public Renderer rend;
    public float floatStrength = 1;

    private float originalY;

    void Start()
    {
        rend = GetComponent<Renderer>();       
        this.originalY = this.transform.position.y;
    }

    void Update()
    {
        //MAKES ITEM FLOAT
        transform.position = new Vector3(transform.position.x,
            originalY + ((float)Math.Sin(Time.time) * floatStrength),
            transform.position.z);

        //MAKES ITEM INVISIBLE UNLESS IN DEFAULTMODE
        var player = GameObject.Find("Player");
		var particle = GameObject.Find("FirstEffect");
        var pos = GameObject.Find("GhostCollect_1");

        if (player.GetComponent<PlayerController>().defaultMode == true)
        {
            rend.enabled = true;
            particle.transform.position = pos.transform.position;
        }

        if (player.GetComponent<PlayerController>().defaultMode == false)
        {
            rend.enabled = false;
            particle.transform.position = new Vector3(0, 1000, 0);
        }

    }
}

