﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Ghostpieces : MonoBehaviour {

	public Text remaining;


    [HideInInspector]
	public float collected;
	public float ghostPieces;
	public float totalPieces;





	// Use this for initialization
	void Start () {

		totalPieces = GameObject.FindGameObjectsWithTag ("GhostPiece").Length;
		remaining.text = "";
	
	}
	
	// Update is called once per frame
	void Update () {



		ghostPieces = GameObject.FindGameObjectsWithTag ("GhostPiece").Length;


		collected = totalPieces - ghostPieces;

		remaining.text = "Soul Pieces: " + collected + "/" + totalPieces;

	
	}
}
