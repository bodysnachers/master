﻿using UnityEngine;
using System.Collections;

public class ReaperAI : MonoBehaviour {

    public Transform playerPos;

    [HideInInspector]
    public bool isChasing = false;
    [HideInInspector]
    public bool hitCheckpoint = false;

    private Patrol walk;
    private float rotSpeed = 3.0f;
    private float movSpeed = 3.0f;
    private Vector3 origPos;
    private Quaternion origRot;

    void Start()
    {
        walk = GetComponent<Patrol>();
        origPos = transform.position;
        origRot = transform.rotation;
        playerPos = GameObject.Find("Player").transform;
    }

    void OnTriggerEnter(Collider other)
    {
        //IF PLAYER IS IN CHASE RANGE BEGIN CHASING
        var enemy = GameObject.Find("Reaper");
        if (other.gameObject.name == "Player")
        {
            isChasing = true;
            walk.enabled = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        //IF PLAYER IS OUT OF CHASE RANGE STOP CHASING
        var enemy = GameObject.Find("Reaper");
        if (other.gameObject.name == "Player")
        {
            isChasing = false;
            walk.enabled = true;
        }
    }

    void Update()
    {
        //CHASING PLAYER
        if (isChasing == true && hitCheckpoint == false)
        {

            //LOOKS AT PLAYER
            transform.rotation =
                Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(playerPos.position - transform.position),
                rotSpeed * Time.deltaTime);

            //CHASES PLAYER
            transform.position += transform.forward * movSpeed * Time.deltaTime;
        }

        //IF PLAYER DIES REAPER POSITION IS RESET
        var player = GameObject.Find("Player");
        if (player.GetComponent<PlayerController>().isDead == true)
        {
            transform.position = origPos;
            transform.rotation = origRot;
        }
    }
}
