﻿using UnityEngine;
using System.Collections;

public class BossAttackRange : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        //IN ATTACK RANGE
        var boss = GameObject.Find("Boss");
        if (other.gameObject.name == "Player")
            boss.GetComponent<BossAI>().inAttackRange = true;
    }

    void OnTriggerExit(Collider other)
    {
        //OUT OF ATTACK RANGE
        var boss = GameObject.Find("Boss");
        if (other.gameObject.name == "Player")
            boss.GetComponent<BossAI>().inAttackRange = false;
    }
}
