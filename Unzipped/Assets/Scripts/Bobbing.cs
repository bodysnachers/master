﻿using UnityEngine;
using System;
using System.Collections;

public class Bobbing : MonoBehaviour
{
    public float floatStrength = 1;

    private float originalY;

    void Start()
    {
        this.originalY = this.transform.position.y;
    }

    void Update()
    {
        //MAKES ITEM FLOAT
        transform.position = new Vector3(transform.position.x,
            originalY + ((float)Math.Sin(Time.time) * floatStrength),
            transform.position.z);

        var log = GameObject.Find("SmallLog2");
        if(log)
            transform.position = new Vector3(transform.position.x,
            originalY + ((float)Math.Sin(Time.time) / floatStrength),
            transform.position.z);
    }
}
