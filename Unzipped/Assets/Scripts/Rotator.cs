﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

    private bool startRotate = false;
    void Update()
    {
        //LILLY PAD ROTATOR SPEED. ADJUST WHAT DELTA TIME IS DIVIDED BY FOR DIFF SPEED.
        transform.Rotate(new Vector3(0, 180, 0) * (Time.deltaTime / 10));
    }
}
