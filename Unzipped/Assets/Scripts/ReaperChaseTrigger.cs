﻿using UnityEngine;
using System.Collections;

public class ReaperChaseTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        //IF PLAYER IS IN CHASE RANGE BEGIN CHASING
        var enemy = GameObject.Find("Reaper");
        if (other.gameObject.name == "Player")
            enemy.GetComponent<ReaperAI>().isChasing = true;
    }

    void OnTriggerExit(Collider other)
    {
        //IF PLAYER IS OUT OF CHASE RANGE STOP CHASING
        var enemy = GameObject.Find("Reaper");
        if (other.gameObject.name == "Player")
            gameObject.GetComponent<ReaperAI>().isChasing = false;
    }
}
