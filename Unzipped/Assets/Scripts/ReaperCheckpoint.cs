﻿using UnityEngine;
using System.Collections;

public class ReaperCheckpoint : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {
        var reaper = GameObject.Find("Reaper");
        if (other.gameObject.tag == "Enemy")
            reaper.GetComponent<ReaperAI>().hitCheckpoint = true;

    }
}
