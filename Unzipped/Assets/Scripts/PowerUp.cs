﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {

	public void OnTriggerEnter(Collider other)
    {

        var player = GameObject.Find("Player");

        if (gameObject.name == ("GhostParticle_1"))
        {
            if (player.GetComponent<PlayerController>().defaultMode && other.gameObject.name == "Player")
                gameObject.SetActive(false);
        }
        else if (other.gameObject.name == "Player")
            gameObject.SetActive(false);
    }
}
