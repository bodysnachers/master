﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartMenu : MonoBehaviour {

    public Canvas quit;
    public Button start;
    public Button exit;
    public AudioClip test;

    private AudioSource source;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }
    void Start()
    {
        source.PlayOneShot(test, 1f);
        quit = quit.GetComponent<Canvas>();
        start = start.GetComponent<Button>();
        exit = exit.GetComponent<Button>();
        quit.enabled = false;
    }

    public void Exit()
    {
        source.PlayOneShot(test, 1f);
        quit.enabled = true;
        start.enabled = false;
        exit.enabled = false;
    }

    public void No() {

        source.PlayOneShot(test, 1f);
        quit.enabled = false;
        start.enabled = true;
        exit.enabled = true;
    }

    public void StartLevel()
    {
        Application.LoadLevel("Game");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
